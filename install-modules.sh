#!/bin/bash

source CONFIG

unset PERL_LOCAL_LIB_ROOT PERL_MB_OPT PERL_MM_OPT PERL5LIB PERL_CPANM_OPT
unset ACKRC

# Install cpanm
PATH=$INSTALL_DIR/bin:$PATH
curl -s -L cpanmin.us | perl - App::cpanminus -q

# Install the modules
cpanm -q < modules.txt

# Get the versions
echo App::cpanminus |
    cat - modules.txt |
    cpanm --info |
    sed -e 's/\.tar\.gz//' |
    sed -e 's!^.*/!!' |
    sed -e 's!-\([^-]*\)$! (\1)!' |
    sed -e 's/-/::/g' |
    sort > versions.txt

echo Done
