#!/bin/bash

source CONFIG

PERL_BUILD_DIR=$BUILD_DIR/perl-$PERL_VERSION

echo Logging to $LOG_FILE
rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR
cat /dev/null > $LOG_FILE

sudo rm -rf $INSTALL_DIR/*
sudo mkdir -p $INSTALL_DIR
sudo chown $USER $INSTALL_DIR

echo Unpacking perl-$PERL_VERSION sources
tar -xvj -C $BUILD_DIR -f $SRC_DIR/perl-$PERL_VERSION.tar.bz2 >>$LOG_FILE

echo Configuring perl-$PERL_VERSION
(
    cd $PERL_BUILD_DIR
    ./Configure -de -Dprefix=$INSTALL_DIR >>$LOG_FILE 2>>$LOG_FILE
)

echo Building perl-$PERL_VERSION
(
    cd $PERL_BUILD_DIR
    JOBS=$[ 2 * $( nproc ) + 1 ]
    make -j$JOBS >>$LOG_FILE 2>>$LOG_FILE
)

echo Installing perl-$PERL_VERSION to $INSTALL_DIR
(
    cd $PERL_BUILD_DIR
    make install >>$LOG_FILE 2>>$LOG_FILE
)


echo Done!
